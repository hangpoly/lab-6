@extends('layouts.master')

@section('content')
<h1>Edit Product</h1>
<div>
    <button class="btn btn-outline-primary "onclick="location.href='/product'">Back</button>
</div>
<br>
<div>
  <form action="/product/{{ $product->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="">Name</label>
      <input value="{{$product->name}}" class="form-control" name="name" type="text" required>
    </div>
    <div class="form-group">
      <label for="">Company</label>
      <select class="custom-select" name="company" value="{{$product->company}}">
        <option value="Apple" {{$product->company == 'Apple' ? 'selected':''}}>Apple</option>
        <option value="Samsung" {{$product->company == 'Samsung' ? 'selected':''}}>Samsung</option>
      </select>
    </div>
    <div class="form-group">
      <label for="">Category</label>
      <select class="custom-select" name="category_id" value="{{$product->category_id}}">
        <option value="1" {{$product->category_id == 1 ? 'selected':''}}>Smart Phone</option>
        <option value="2" {{$product->category_id == 2 ? 'selected':''}}>Laptop</option>
      </select>
    </div>
    <div>
      <button type="submit" class="btn btn-outline-success">Update</button>
      <button type="button" class="btn btn-danger" onclick="location.href='/product'">Cancel</button>
    </div>
  </form>
</div>

@endsection