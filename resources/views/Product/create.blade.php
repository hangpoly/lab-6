@extends('layouts.master')

@section('content')
<h1>Edit Product</h1>
<div>
    <button class="btn btn-outline-primary "onclick="location.href='/product'">Back</button>
</div>
<br>
<div>
  <form action="/product" method="POST">
    @csrf
    @method('POST')
    <div class="form-group">
      <label for="">Name</label>
      <input class="form-control" name="name" type="text" required>
    </div>
    <div class="form-group">
      <label for="">Company</label>
      <select class="custom-select" name="company" required>
        <option value="Apple">Apple</option>
        <option value="Samsung">Samsung</option>
      </select>
    </div>
    <div class="form-group">
      <label for="">Category</label>
      <select class="custom-select" name="category_id" required>
        <option value="1">Smart Phone</option>
        <option value="2">Laptop</option>
      </select>
    </div>
    <div>
      <button type="submit" class="btn btn-outline-success">Create</button>
      <button type="button" class="btn btn-danger" onclick="location.href='/product'">Cancel</button>
    </div>
  </form>
</div>

@endsection