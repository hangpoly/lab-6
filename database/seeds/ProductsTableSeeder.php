<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'iphone 11',
            'company' => 'Apple',
            'category_id' => 1
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung Galaxy S10',
            'company' => 'Samsung',
            'category_id' => 1
        ]);

        DB::table('products')->insert([
            'name' => 'Macbook Pro 11',
            'company' => 'Apple',
            'category_id' => 2
        ]);
    }
}
